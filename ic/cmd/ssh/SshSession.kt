package ic.cmd.ssh


import java.io.IOException
import java.util.*

import com.jcraft.jsch.*

import ic.base.strings.ext.asText
import ic.base.strings.ext.replaceAll
import ic.base.throwables.IoException
import ic.cmd.bash.BashSession
import ic.stream.input.ByteInput
import ic.stream.input.ext.asInputStream
import ic.stream.input.ext.read
import ic.stream.input.fromis.ByteInputFromInputStream
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toString
import ic.text.Text
import ic.util.auth.BasicAuth
import ic.util.log.logD
import ic.util.text.charset.Charset.Companion.Utf8


class SshSession (host: String, auth: BasicAuth<String?, String?>) : BashSession() {

	override var workdirPath : String = "~"

	private var session: Session

	init {
		logD("SshSession") { "init host: $host, username: ${ auth.username }, password: ${ auth.password }" }
		try {
			val jsch = JSch()
			session = (
				if (auth.username == null) {
					jsch.getSession(host)
				} else {
					jsch.getSession(auth.username, host)
				}
			)
			if (auth.password != null) {
				session.setPassword(auth.password)
			}
			val config = Properties()
			config["StrictHostKeyChecking"] = "no"
			session.setConfig(config)
			session.connect()
		} catch (e: JSchException) {
			throw IoException.Runtime(e)
		}
	}

	@Synchronized
	override fun implementExecuteScript (script: String): Text {
		logD("SshSession") { "script: $script" }
		try {
			val channel = session.openChannel("exec") as ChannelExec
			try {
				channel.setCommand(script)
				channel.connect()
				val inputStream: ByteInput = ByteInputFromInputStream(channel.inputStream)
				val errorStream: ByteInput = ByteInputFromInputStream(channel.errStream)
				val responseString = (
					inputStream.read().toString(Utf8) +
					errorStream.read().toString(Utf8)
				).replaceAll("bash: line 0: ", "")
				inputStream.close()
				errorStream.close()
				logD("SshSession") { "responseString: $responseString" }
				return responseString.asText
			} catch (e: IOException) {
				throw IoException.Runtime(e)
			} finally {
				channel.disconnect()
			}
		} catch (e: JSchException) {
			throw IoException.Runtime(e)
		}
	}

	fun uploadFile (byteSequence: ByteSequence, targetPath: String) {
		val channel = session.openChannel("sftp") as ChannelSftp
		try {
			channel.connect()
			channel.put(byteSequence.newIterator().asInputStream, targetPath)
		} finally {
			channel.disconnect()
		}
	}

	@Synchronized
	override fun close() {
		session.disconnect()
	}

}